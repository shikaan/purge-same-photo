import unittest
import face_recognition
import glob


class TestHasFaces(unittest.TestCase):

    def test_face_photo(self):
        self.assertEqual(face_recognition.has_faces('./photo_sample/face1.jpg'), True)

    def test_no_face_photo(self):
        self.assertEqual(face_recognition.has_faces('./photo_sample/no-face1.jpg'), False)

    def test_bulk(self):
        no_faces = glob.iglob('./photo_sample/no-face*')
        faces = glob.iglob('./photo_sample/face*')

        for photo in faces:
            self.assertEqual(face_recognition.has_faces(photo), True)

        for photo in no_faces:
            self.assertEqual(face_recognition.has_faces(photo), False)


class TestSameFaces(unittest.TestCase):

    def test_one_face_photo_true(self):
        one = './photo_sample/face1.jpg'
        two = './photo_sample/same-face1.jpg'
        self.assertEqual(face_recognition.same_face(one, two), True)

    def test_one_face_photo_false(self):
        one = './photo_sample/face1.jpg'
        two = './photo_sample/same-face4.jpg'
        self.assertEqual(face_recognition.same_face(one, two), False)

    def test_bulk_false(self):
        same_faces = glob.iglob('./photo_sample/same-face*')
        faces = glob.iglob('./photo_sample/face*')

        for f in faces:
            for sf in same_faces:
                if f[-5] == sf[-5]:
                    self.assertEqual(face_recognition.same_face(f, sf), True)
                else:
                    self.assertEqual(face_recognition.same_face(f, sf), False)


hasFacesSuite = unittest.TestLoader().loadTestsFromTestCase(TestHasFaces)
sameFaceSuite = unittest.TestLoader().loadTestsFromTestCase(TestSameFaces)

suite = unittest.TestSuite([hasFacesSuite, sameFaceSuite])

unittest.TextTestRunner(verbosity=2).run(suite)
