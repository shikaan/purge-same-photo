import unittest
import os
import glob
import random
import cluster_maker as main

class TestCreateClusters(unittest.TestCase):
    ''' Unit tests for the clusters maker '''
    def create_empty_mocks_array(self, size):
        mock_array = []
        for i in range(0, size):
            mock_array.append(open('./sample/file' + str(i), 'a'))
        return mock_array

    def setUp(self):
        sample_files = glob.iglob('./sample/*')
        for item in sample_files:
            os.remove(item)

    def test_two_files_one_cluster(self):
        temp = self.create_empty_mocks_array(2)
        temp[0].write('0123456789')
        temp[0].close()
        temp[1].write('0123456789')
        temp[1].close()

        expectation = {'10': ['file1', 'file0']}

        self.assertEqual(main.create_clusters('./sample/'), expectation)

    def test_four_files_two_clusters(self):
        temp = self.create_empty_mocks_array(4)
        temp[0].write('0123456789')
        temp[0].close()
        temp[1].write('0123456789')
        temp[1].close()
        temp[2].write('01234567')
        temp[2].close()
        temp[3].write('01234567')
        temp[3].close()

        expectation = {'10': ['file1', 'file0'], '8': ['file3', 'file2']}

        self.assertEqual(main.create_clusters('./sample/'), expectation)

    def test_random_files_random_clusters(self):
        size = random.randint(0, 100)
        temp = self.create_empty_mocks_array(size)
        expectation = {}
        for i in temp:
            length = random.randint(0, 100)
            for j in range(0, length):
                i.write('a')

            i.close()

            if length not in expectation.keys():
                expectation[length] = [i.name.split('/')[-1]]
            else:
                expectation[length].append(i.name.split('/')[-1])

        self.assertEqual(main.create_clusters('./sample/').keys().sort(), expectation.keys().sort())

createClustersSuite = unittest.TestLoader().loadTestsFromTestCase(TestCreateClusters)

suite = unittest.TestSuite([createClustersSuite])

unittest.TextTestRunner(verbosity=2).run(suite)
