"""
    Unit tests for cluster cleaner.
"""
import unittest
import os
import glob
import random
import main as main


class TestClearUnitClusters(unittest.TestCase):

    def test_empty_dictionary_with_one_unit_cluster(self):
        mock = {'11358': ['photo.jpg']}
        self.assertEqual(main.clear_unit_clusters(mock), {})

    def test_empty_dictionary_w_more_unit_clusters(self):
        mock = {'11358': ['photo.jpg'], '1135': ['photo.jpg']}
        self.assertEqual(main.clear_unit_clusters(mock), {})

    def test_non_empty_dictionary_with_one_larger_cluster(self):
        mock = {'11358': ['photo.jpg', 'foo.bar'],
                '1135': ['photo.jpg'],
                '4135': ['photo.jpg']}
        self.assertEqual(len(main.clear_unit_clusters(mock).keys()), 1)

    def test_non_empty_dictionary_with_more_larger_clusters(self):
        mock = {'11358': ['photo.jpg', 'foo.bar'],
                '1135': ['photo.jpg', 'foo.bar'],
                '4135': ['photo.jpg']}
        self.assertEqual(len(main.clear_unit_clusters(mock).keys()), 2)

    def test_returns_a_cluster(self):
        self.assertTrue(type(main.clear_unit_clusters({})) is dict)

clearUnitClustersSuite = unittest.TestLoader().loadTestsFromTestCase(TestClearUnitClusters)

suite = unittest.TestSuite([clearUnitClustersSuite])

unittest.TextTestRunner(verbosity=2).run(suite)
