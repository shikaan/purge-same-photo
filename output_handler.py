"""
    Creates the output folder with only useful photos in it
"""
from os import mkdir, rmdir
from os.path import join, isdir
from shutil import copy

def make_folder_with_results(dir, clusters):
    ''' Creates a folder named results in cwd with results of filtering'''
    if isdir('results'):
        rmdir('results')
    mkdir('results')
    for key in clusters.keys():
        for item in clusters[key]:
            copy(join(dir, item), 'results')
