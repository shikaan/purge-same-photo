"""
    This service is meant to clean clusters
"""
from os.path import join
from itertools import combinations
from face_recognition import same_face, has_faces

def clear_unit_clusters(clusters):
    '''	Removes clusters of only one element from clusters dictionary '''
    for key in clusters.keys():
        if len(clusters[key]) < 2:
            clusters.pop(key)

    return clusters

def filter_faceless_files(directory, clusters):
    ''' Removes files without faces from clusters '''
    for key in clusters.keys():
        clusters[key] = [item for item in clusters[key] if has_faces(join(directory, item))]
        if len(clusters[key]) == 0:
            clusters.pop(key)

    return clusters

def filter_dupes(directory, clusters):
    ''' Removes files with same faces in clusters '''
    for key in clusters.keys():
        for pair in combinations(clusters[key], r=2):
            print('looking for dupes of: ', pair[1])
            if same_face(join(directory, pair[0]), join(directory, pair[1])):
                clusters[key].remove(pair[0])

    return clusters
