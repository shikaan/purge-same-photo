"""
    This service is meant to create cluster by size.
"""

from os import listdir
from os.path import getsize, join

def create_clusters(dir):
    '''	Creates clusters by size of files and returns a
        dictionary with size as keys and list of files as
        values '''
    files = listdir(dir)
    clusters = {}

    for file in files:
        size = getsize(join(dir, file))
        if str(size) not in clusters.keys():
            clusters[str(size)] = [file]
        else:
            clusters[str(size)].append(file)

    return clusters
