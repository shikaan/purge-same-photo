"""
    Application controller
"""
import sys
from cluster_maker import create_clusters
from cluster_cleaner import clear_unit_clusters, filter_faceless_files, filter_dupes
from output_handler import make_folder_with_results

DIRECTORY = sys.argv[1]

print("Creating clusters")
CLUSTERS = create_clusters(DIRECTORY)
print("Filtering faceless entries")
CLUSTERS = clear_unit_clusters(CLUSTERS)
CLUSTERS = filter_faceless_files(DIRECTORY, CLUSTERS)
CLUSTERS = clear_unit_clusters(CLUSTERS)
print("Filtering items with same face")
CLUSTERS = filter_dupes(DIRECTORY, CLUSTERS)

make_folder_with_results(DIRECTORY, CLUSTERS)
