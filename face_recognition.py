"""
    This handles face recognition functions. Uses openCV.
"""
import cv2

FACE_CASCADE = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')

def has_faces(image_path):
    '''Returns true if there's a face in the image provided'''

    print('Looking for faces in ', image_path)
    # Read the image
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Detect faces in the image
    faces = FACE_CASCADE.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    return len(faces) > 0


def same_face(image_path_1, image_path_2):
    '''Returns true if there's the same face in both the imahes provided'''
    image1 = cv2.imread(image_path_1)
    image2 = cv2.imread(image_path_2)
    gray1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)

    faces1 = FACE_CASCADE.detectMultiScale(
        gray1,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    faces2 = FACE_CASCADE.detectMultiScale(
        gray2,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    return faces1[0].tolist() == faces2[0].tolist()
